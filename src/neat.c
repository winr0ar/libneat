#define ALIGN_DEBUG
#define ALIGN_DEBUG_VERBOSE
#define ALIGN_TRACEBACK


#include "../include/neat.h"
//#include "_cgo_export.h"
#ifdef ALIGN_DEBUG
#include <stdio.h>
#endif
#include <stdlib.h>
#include <limits.h>

// Bench times
// Set to 1
// If you want to repeat the process for benchmark, then set to something like 10 or 50.
#define BENCH_TIMES 1

#define TYPE_DNA 0x01
#define TYPE_PROT 0x02

// Tile size
// Each thread needs 2*(Tile_SZ+1) bytes for temporary storage
#define TILE_SZ 3
#define MAX_SZ 65535

// Origin tiles
// In the case of tile size < 128,
// The origin tile position is 8 bit
// The leftmost bit is the direction, where 1 = top, 0 = left
// The remaining 7 bit is the cell number
#if MAX_SZ < 128
typedef unsigned char origin_tile_pos;
#define TRACE_ORIGIN_BIT 0x80
#define TRACE_ORIGIN_MASK 0x7F
#else
typedef unsigned int origin_tile_pos;
#define TRACE_ORIGIN_BIT 0x8000
#define TRACE_ORIGIN_MASK 0x7FFF
#endif

// Penalties
// Currently it doesn't support matrices for scores, just simple scores
#define PENALTY_GAP -10
// TODO REMOVE THIS
#define PENALTY_MISMATCH -4
#define PENALTY_MATCH 5


/* Aligns a part of the matrix */

/* The algorithm works as follow
 * 
 * +---+---+---+
 * | 0 | 2 | 4 |       <-- Tiles
 * +---+---+---+
 * | 1 | 3 | 5 |
 * +---+---+---+
 * 
 * It will divide the alignment matrix to tiles of sizes (TILE_SZ+1)^2
 * and stores the scores on the border that it just calculated 
 * in alignment_score_top and alignment_score_left
 * After calculated the first tile, the scores will look like this:
 * 
 * (Note that the score_top and score_left scores are not 
 * straight lines!)
 * 
 * +   +---+---+        <-- Score_top
 *   0 | 2   4  
 * +---+   +   +
 * | 1   3   5  
 * +   +   +   +
 * ^ Score_left
 * 
 * To do traceback, each small cell on each tile is stored on the trace_origin
 * array in the order it was calculated. It would be 
 * origin_direction followed by n bits for origin_tile_position
 * */
int align_tile(
	int work_type,
	char * inseq1, char * inseq2, 
	unsigned int inseq1_l, unsigned int inseq2_l, 
	unsigned short tile_length, 
	int * score_matrix, 
	int * alignment_score_top,
	int * alignment_score_left,
	unsigned int tile_x,
	unsigned int tile_y,
	origin_tile_pos * trace_origin_top,
	origin_tile_pos * trace_origin_left
) {
	unsigned int length_x = tile_length;
	unsigned int length_y = tile_length;
	
	// The temporary alignment scores that contains the current row 
	// and previous row values looks like this

	// [ ] [ ] [*] [p] [p] [p] [p] [p]  <-- Previous row
	// [w] [w] [w] [o] [ ] [ ] [ ] [ ]  <-- Working row

	// To calculate the value of the current cell (marked o) you only need:
	// - The previous row's left value marked [*]
	// - The previous row's top value, the leftmost [p]
	// - The working row's left value, the rightmost [w]

	// I store my temporary working space as 
	// [w] [w] [w] [p] [p] [p] [p] [p] and the previous row's left value [*]
	int tmp_align_scores[TILE_SZ + 1];
	#ifdef ALIGN_TRACEBACK
	origin_tile_pos tmp_trace_origin[TILE_SZ + 1];
	origin_tile_pos traceback_left;
	#endif
	// The lookback left is the previous row's left value
	int lookback_left;
	
	
	int tmp = 0;
	#ifdef ALIGN_TRACEBACK
	origin_tile_pos trace_tmp;
	#endif
	unsigned offset_top = length_x * tile_x;
	unsigned offset_left = length_y * tile_y;
	
	if (offset_top + length_x > inseq1_l) {
		length_x = inseq1_l - offset_top;
	}
	
	if (offset_left + length_y > inseq2_l) {
		//printf("offset left %d, length y %d, inseq1_l %d, inseq2_l - offset_left = %d \n", offset_left, length_y, inseq1_l, (unsigned int)(inseq2_l - offset_left));
		length_y = inseq2_l - offset_left;
	}
	
	// l_x and l_y is the actual resulting matrix size, 1 greater than the seq lengths
	unsigned int l_x = length_x + 1;
	unsigned int l_y = length_y + 1;
	
	int i,row,col;
	int score_fromleft, score_fromtop;
	#ifdef ALIGN_DEBUG
	printf("Aligning ");
	for (i = 0; i < length_x; ++ i) {
		putchar(inseq1[i + offset_top]);
	}
	
	printf(" (len=%d) and ", length_x);
	for (i = 0; i < length_y; ++ i) {
		putchar(inseq2[i + offset_left]);
	}
	printf(" (len=%d)\n", length_y);
	
	printf("Got score top = ");
	for (i = 0; i < l_x; ++ i)  {
		printf(
			"%d [%s%d], ", 
		 alignment_score_top[i + offset_top], 
		#ifdef ALIGN_TRACEBACK
		 trace_origin_top[i + offset_top] & TRACE_ORIGIN_BIT ? "t" : "l",
		 trace_origin_top[i + offset_top] & TRACE_ORIGIN_MASK
		#else 
		 "", 0
		#endif
		);
		
	}
	
	puts("\nleft = ");
	for (i = 0; i < l_y; ++ i)  {
		//printf("%d, ", alignment_score_left[i + offset_left]);
		printf(
			"%d [%s%d], ", 
		 alignment_score_left[i + offset_left], 
		 #ifdef ALIGN_TRACEBACK
		 trace_origin_left[i + offset_left] & TRACE_ORIGIN_BIT ? "t" : "l",
		 trace_origin_left[i + offset_left] & TRACE_ORIGIN_MASK
		 #else 
		 "", 0
		 #endif
		);
	}
	printf("\n");
	#endif
	
	// We don't want to copy the last element of each top and left vector
	// unless it's the final one
	unsigned int offset = 1;
	if (offset_left + length_y >= inseq2_l) {
		offset = 0;
		#ifdef ALIGN_DEBUG
		printf("Last tile in a vertical scan - increasing copy length!\n");
		#endif
	}
	
	// copy the alignment matrix to the temporary alignment matrix
	for (i = 0; i < l_x; ++ i)  {
		tmp_align_scores[i] = alignment_score_top[i + offset_top];
		#ifdef ALIGN_TRACEBACK
		tmp_trace_origin[i] = trace_origin_top[i + offset_top];
		#endif
	}
	
	// first row processing, messy
	
	if (offset_left != 0) {
		lookback_left = alignment_score_left[0 + offset_left];
		#ifdef ALIGN_TRACEBACK
		traceback_left = trace_origin_left[0 + offset_left];
		#endif
	} else {
		// This is to fix a corner case where it would copy 
		// over the first result from the last tile which has been overwritten
		// by the last sweep
		
		#ifdef ALIGN_DEBUG
		printf("Fixing the top score as it's wrong: %d\n", offset_top * PENALTY_GAP);
		#endif
		
		lookback_left = offset_top * PENALTY_GAP;
		#ifdef ALIGN_TRACEBACK
		traceback_left = TRACE_ORIGIN_BIT & ((origin_tile_pos) offset_top);
		#endif
	}
	
	// Store the result to alignment_score_left for the first left element (?)
	// Can't explain now, I must have been out of my mind, but this works...
	alignment_score_left[0 + offset_left] = tmp_align_scores[l_x-1];
	
	#ifdef ALIGN_TRACEBACK
	trace_origin_left[0 + offset_left] = tmp_trace_origin[l_x-1];
	#endif
	
	// end first row processing
	
	for (row = 1; row < l_y; ++ row) {
		// First, get the left result
		lookback_left = tmp_align_scores[0];
		#ifdef ALIGN_TRACEBACK
		traceback_left = tmp_trace_origin[0];
		#endif
		
		tmp_align_scores[0] = alignment_score_left[row + offset_left];
		#ifdef ALIGN_TRACEBACK
		tmp_trace_origin[0] = trace_origin_left[row + offset_left];
		#endif
		
		#ifdef ALIGN_DEBUG_VERBOSE
		puts("This iteration score top = ");
		for (i = 0; i < l_x; ++ i)  {
			printf(
					"%d [%s%d], ", 
				tmp_align_scores[i], 
				#ifdef ALIGN_TRACEBACK
				tmp_trace_origin[i] & TRACE_ORIGIN_BIT ? "t" : "l",
				tmp_trace_origin[i] & TRACE_ORIGIN_MASK
				#else 
				"", 0
				#endif
				);
			//printf("%d, ", tmp_align_scores[i ]);
			
		}
		printf("lookback left= %d [%s%d]", lookback_left, 
			   #ifdef ALIGN_TRACEBACK
			   traceback_left & TRACE_ORIGIN_BIT ? "t" : "l",
				traceback_left & TRACE_ORIGIN_MASK
			   #else 
				"", 0
				#endif
			  );
		
		puts("\n");
		#endif
		
		for (col = 1; col < l_x; ++ col) {
			score_fromleft = tmp_align_scores[col - 1] + PENALTY_GAP;
			score_fromtop = tmp_align_scores[col] + PENALTY_GAP;
			
			tmp = lookback_left;
			#ifdef ALIGN_TRACEBACK
			trace_tmp = traceback_left;
			#endif
			
			if (inseq1[offset_top + col -1] == inseq2[offset_left + row -1])  {
				tmp += PENALTY_MATCH;
			} else {
				tmp += PENALTY_MISMATCH;
			}
			
			if (score_fromleft > tmp) {
				tmp = score_fromleft;
				#ifdef ALIGN_TRACEBACK
				trace_tmp = tmp_trace_origin[col -1];
				#endif
			}
			
			if (score_fromtop > tmp) {
				tmp = score_fromtop;
				#ifdef ALIGN_TRACEBACK
				trace_tmp = tmp_trace_origin[col];
				#endif
			}
			tmp_align_scores[col] = tmp;
			#ifdef ALIGN_TRACEBACK
			lookback_left = tmp_align_scores[col];
			tmp_trace_origin[col] = trace_tmp;
			#endif
		}
		// Save the current right values because we're gonna discard it.
		if (row < l_y - offset) {
			alignment_score_left[row + offset_left] = tmp_align_scores[l_x-1];
			#ifdef ALIGN_TRACEBACK
			trace_origin_left[row + offset_left] = tmp_trace_origin[l_x-1];
			#endif
		}
		
		#ifdef ALIGN_DEBUG_VERBOSE
		puts("This iteration aftermatch score top = ");
		for (i = 0; i < l_x; ++ i)  {
			printf(
					"%d [%s%d], ", 
				tmp_align_scores[i], 
				#ifdef ALIGN_TRACEBACK
				tmp_trace_origin[i] & TRACE_ORIGIN_BIT ? "t" : "l",
				tmp_trace_origin[i] & TRACE_ORIGIN_MASK
				#else 
				"",0
				#endif
				);
			//printf("%d, ", tmp_align_scores[i ]);
			
		}
		/*
		printf("lookback left= %d [%s%d]", lookback_left, traceback_left & TRACE_ORIGIN_BIT ? "t" : "l",
				traceback_left & TRACE_ORIGIN_MASK);
		*/
		puts("\n");
		#endif
	}
	
	// This is prev_row as the prev_row contains the last calculation result 
	// we have, thus it should have the latest row values.
	for (i = 0; i < l_x; ++ i)  {
		alignment_score_top[i + offset_top] = tmp_align_scores[i];
		#ifdef ALIGN_TRACEBACK
		trace_origin_top[i + offset_top] = tmp_trace_origin[i];
		#endif
	}
	
	#ifdef ALIGN_DEBUG
	puts("Calc. score top = ");
	for (i = 0; i < l_x; ++ i)  {
		printf(
			"%d [%s%d], ", 
		 alignment_score_top[i + offset_top], 
		 #ifdef ALIGN_TRACEBACK
		 tmp_trace_origin[i] & TRACE_ORIGIN_BIT ? "t" : "l",
		 tmp_trace_origin[i] & TRACE_ORIGIN_MASK
		 #else 
		 "", 0
		 #endif
		);
		
	}
	
	puts("\nleft = ");
	for (i = 0; i < l_y; ++ i)  {
		//printf("%d, ", alignment_score_left[i + offset_left]);
		printf(
			"%d [%s%d], ", 
		 alignment_score_left[i + offset_left], 
		 #ifdef ALIGN_TRACEBACK
		 trace_origin_left[i] & TRACE_ORIGIN_BIT ? "t" : "l",
		 trace_origin_left[i] & TRACE_ORIGIN_MASK
		 #else 
		 "",0
		 #endif
		);
	}
	puts("\n");
	#endif
	
	return tmp;
}

int max_3( int a, int b, int c )
{
	int temp = a>b ? a: b; 
	return c>temp ? c: temp; 
}

int neat_needleman_ref(char *seq1, 
					   char *seq2, 
					   unsigned int seq1_len, 
					   unsigned int seq2_len)
{
	int dia, up, left;
	int i, k, j;
	int * matrix;
	int iteration;
	
	matrix = (int *) malloc((seq1_len +1) * (seq2_len +1) * sizeof(int));
	for (i=0; i<(seq1_len +1) * (seq2_len +1); ++ i) {
		matrix[i] = 0;
	}
	#if BENCH_TIMES > 1
	for (iteration = 0; iteration < BENCH_TIMES ; ++ iteration) {
		#endif
		for(j = 1; j <= seq1_len; ++j)
			matrix[j] = PENALTY_GAP * j;
		for(j = 1; j <= seq2_len; ++j)
			matrix[j*(seq1_len+1)+0] = PENALTY_GAP * j;
		
		//fill the alignment matrix
		for(k = 1; k <= seq2_len; ++k){           
			for(j = 1; j <= seq1_len; ++j){			
				dia = matrix[(k-1)*(seq1_len+1)+j-1] + ((seq2[k-1] == seq1[j-1]) ? PENALTY_MATCH  : PENALTY_MISMATCH);
				up	= matrix[(k-1)*(seq1_len+1)+j] + PENALTY_GAP;
				left= matrix[k*(seq1_len+1)+j-1] + PENALTY_GAP;
				matrix[k*(seq1_len+1)+j] = max_3(left, dia, up);
			}
		}
		#if BENCH_TIMES > 1
		
	}
	#endif
	#ifdef ALIGN_DEBUG
	int col, row;
	printf("ALIGNMENT MATRIX\n%4c ", ' ');
	for (col = 0; col < seq1_len + 1; ++ col) { 
		if (col > 0) {
			printf("%4c ", seq1[col -1]);
		} else {
			printf("%4c ", '.');
		}
	}
	printf("\n");
	
	for (row = 0; row < seq2_len +1; ++ row) {
		if (row > 0) {
			printf("%4c ", seq2[row -1]);
		} else {
			printf("%4c ", '.');
		}
		for (col = 0; col < seq1_len + 1; ++ col) {
			printf("%4d ", matrix[col + row * (seq1_len+1)]);
		}
		printf("\n");
	}
	#endif
	
	int ret = matrix[seq2_len*(seq1_len+1)+seq1_len];
	free (matrix);
	return ret;
}


int neat_needleman(
	int work_type,
	char * inseq1, char * inseq2, 
	unsigned int length1,
	unsigned int length2,
	int * score_matrix
) {
	int i, iteration = 0;
	int last_return = 0;
	unsigned int tile_x, tile_y;
	/*
	// First we create the temporary matrices to work on
	int * tmp_alignment_matrix = (int *)  malloc((TILE_SZ +1) * 2 * sizeof(int));
	*/
	
	// Now create the top and left alignment matrix to work on
	int * alignment_score_top = (int *)  malloc((length1 + 1) * sizeof(int));
	int * alignment_score_left = (int *)  malloc((length2 +1) * sizeof(int));
	#ifdef ALIGN_TRACEBACK
	origin_tile_pos * alignment_trace_top = (origin_tile_pos *)  malloc((length1 + 1) * sizeof(origin_tile_pos));
	origin_tile_pos * alignment_trace_left = (origin_tile_pos *)  malloc((length2 +1) * sizeof(origin_tile_pos));
	#endif
	// Now work on this one tile at a time
	unsigned int work_x = length1 / TILE_SZ;
	if ((length1 % TILE_SZ) != 0) {
		work_x ++;
	}
	unsigned int work_y = length2 / TILE_SZ;
	if ((length2 % TILE_SZ) != 0) {
		work_y ++;
	}
	#if BENCH_TIMES > 1
	for (iteration = 0; iteration < BENCH_TIMES; ++ iteration) {
		#endif		
		for (i = 0; i < length1 + 1; ++ i) {
			alignment_score_top[i] = PENALTY_GAP * i;
			#ifdef ALIGN_TRACEBACK
			alignment_trace_top[i] = TRACE_ORIGIN_BIT | ((origin_tile_pos) i);
			#endif
		}
		
		for (i = 0; i < length2 + 1; ++ i) {
			alignment_score_left[i] = PENALTY_GAP * i;
			#ifdef ALIGN_TRACEBACK
			alignment_trace_left[i] = i;
			#endif
		}
		
		#ifdef ALIGN_DEBUG
		printf("### Got work aligning %s (len=%d) and %s (len=%d)\n", inseq1, length1, inseq2, length2);
		#endif
		
		#ifdef ALIGN_DEBUG
		puts("Now it looks like this top=");
		for (i = 0; i < length1+1; ++ i)  {
				printf(
					"%d [%s%d], ", 
					alignment_score_top[i], 
					#ifdef ALIGN_TRACEBACK
					alignment_trace_top[i] & TRACE_ORIGIN_BIT ? "t" : "l",
					alignment_trace_top[i] & TRACE_ORIGIN_MASK
					#else
					"",0
					#endif
				);
		}
		
		puts("\nleft = ");
		for (i = 0; i < length2+1; ++ i)  {
				printf(
					"%d [%s%d], ", 
					alignment_score_left[i], 
					#ifdef ALIGN_TRACEBACK
					alignment_trace_left[i] & TRACE_ORIGIN_BIT ? "t" : "l",
					alignment_trace_left[i] & TRACE_ORIGIN_MASK
					#else
					"",0
					#endif
				);
		}
		puts("\n");
		#endif
		
		for (tile_x = 0; tile_x < work_x; ++ tile_x) {
			for (tile_y =0; tile_y < work_y; ++ tile_y) {
				#ifdef ALIGN_DEBUG
				printf("-----\nIteration #%d start x = %d, start y = %d\n", iteration++, tile_x * TILE_SZ, tile_y * TILE_SZ);
				#endif
				last_return = align_tile (
					work_type,
					inseq1,
					inseq2, 
					length1,
					length2,
					TILE_SZ, 
					score_matrix, 
					alignment_score_top,
					alignment_score_left,
					tile_x,
					tile_y,
					#ifdef ALIGN_TRACEBACK
					alignment_trace_top,
					alignment_trace_left
					#else
					0, 0
					#endif
				);
				
				#ifdef ALIGN_DEBUG_VERBOSE
				puts("Now it looks like this top=");
				for (i = 0; i < length1+1; ++ i)  {
					printf("%d, ", alignment_score_top[i]);
				}
				
				puts("\nleft = ");
				for (i = 0; i < length2+1; ++ i)  {
					printf("%d, ", alignment_score_left[i]);
				}
				puts("\n");
				#endif
			}
		}
		#if BENCH_TIMES > 1
	}
	#endif
	
	free(alignment_score_top);
	free(alignment_score_left);

	#ifdef ALIGN_TRACEBACK
	free(alignment_trace_top);
	free(alignment_trace_left);
	#endif
	
	return last_return;
}
