libneat
=======

CUDA DNA and protein aligner, supposedly fast...

Current implementation phase: CPU

HOW TO INSTALL
==============

To install the core libneat, go to the src directory and issue 
	make && make install

To install the test suite, do the following:

1. Install Go

2. Issue 

    go get code.google.com/p/biogo

3. Go to the go_src directory and issue 

    go build

4. Bench time!

    ../go_src/go_src -a ../tests/real1.txt
