#ifndef __LIBNEAT_H__
#define __LIBNEAT_H__

int neat_needleman(
    int work_type,
    char * inseq1, char * inseq2, 
    unsigned int length1,
    unsigned int length2,
    int * score_matrix
    );

int neat_needleman_ref(char *seq1, 
				   char *seq2, 
				   unsigned int seq1_len, 
				   unsigned int seq2_len);

#endif