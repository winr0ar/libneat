package main

/*
#cgo LDFLAGS: -lneat
#include "../include/neat.h"
*/
import "C"

//import "unsafe"

import (
	"code.google.com/p/biogo/align"
	"code.google.com/p/biogo/alphabet"
	//"code.google.com/p/biogo/io/seqio"
	"code.google.com/p/biogo/io/seqio/fasta"
	"code.google.com/p/biogo/seq/linear"
	//"encoding/json"
	"flag"
	"fmt"
	//"io"
	"log"
	"os"
	//"os/exec"
	//"os/signal"
	//"strings"
	"time"
)

func trace(s string) (string, time.Time) {
	log.Printf("trace start: %s\n", s)
	return s, time.Now()
}

func un(s string, startTime time.Time) {
	elapsed := time.Since(startTime)
	log.Printf("trace end: %s, elapsed %f secs\n", s, elapsed.Seconds())
}

func main() {

	var fa_n *string = flag.String("a", "a.fasta", "First sequence file")
	//var fb_n *string = flag.String("b", "b.fasta", "Second sequence file")
	//var f_type *string = flag.String("t", "dna", "Type: (dna) or (prot)ein")

	flag.Parse()

	seqtype := alphabet.DNA

	f1, err := os.Open(*fa_n)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Error: %v.", err)
		os.Exit(1)
	}
	defer f1.Close()

	in1 := fasta.NewReader(f1, linear.NewSeq("", nil, seqtype))
	/*
		f2, err := os.Open(*fb_n)
		if err != nil {
			fmt.Fprintf(os.Stderr, "Error: %v.", err)
			os.Exit(1)
		}
		defer f2.Close()
		in2 := fasta.NewReader(f2, linear.NewSeq("", nil, seqtype))
	*/

	for true {

		var f1seq, err1 = in1.Read()
		var f2seq, err2 = in1.Read()

		if err1 != nil && err2 != nil {
			os.Exit(0)
		}

		//  	 A	 C	 G	 T	 -
		// A	10	-3	-1	-4	-5
		// C	-3	 9	-5	 0	-5
		// G	-1	-5	 7	-3	-5
		// T	-4	 0	-3	 8	-5
		// -	-5	-5	-5	-5	 0
		needle := align.NW{
			{5, -4, -4, -4, -10},
			{-4, 5, -4, -4, -10},
			{-4, -4, 5, -4, -10},
			{-4, -4, -4, 5, -10},
			{-10, -10, -10, -10, 0},
		}

		fmt.Printf("\n\n--------------------\n")
		fmt.Printf("Seq 1: %-s\n", f1seq)
		fmt.Printf("Seq 2: %-s\n", f2seq)

		f1seqs := fmt.Sprintf("%-s", f1seq)
		f2seqs := fmt.Sprintf("%-s", f2seq)

		cs1 := C.CString(f1seqs) // C.CString("AAATTT")
		cs2 := C.CString(f2seqs) // C.CString("AAAGGG")

		ret1 := 0
		ret2 := 0
		func() {
			fmt.Printf("Doing CPU alignment - Biogo package...\n")
			defer un(trace("BIOGOBENCH"))
			for i := 0; i < 1; i++ {
				alg, err := needle.Align(f1seq, f2seq)
				if err == nil {
					//fmt.Printf("%s\n", alg)
					fa := align.Format(f1seq, f2seq, alg, '-')
					fmt.Printf("%s\n%s\n", fa[0], fa[1])
				}
			}
		}()

		func() {
			fmt.Printf("Doing CPU alignment - Huan's extended biogo glue...\n")
			defer un(trace("HUANBENCH"))
			for i := 0; i < 1; i++ {
				ret1 = int(C.neat_needleman(0, cs1, cs2, C.uint(len(f1seqs)), C.uint(len(f2seqs)), nil))
			}
		}()

		func() {
			fmt.Printf("Doing CPU alignment - Huan's needleman reference...\n")
			defer un(trace("REFBENCH"))
			for i := 0; i < 1; i++ {
				ret2 = int(C.neat_needleman_ref(cs1, cs2, C.uint(len(f1seqs)), C.uint(len(f2seqs))))
			}
		}()

		fmt.Printf("Done bench ret1=%d, ret2=%d!\n", ret1, ret2)

	}
}
